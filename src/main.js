import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from './router'
import VueExpandableImage from 'vue-expandable-image'
import VueSession from 'vue-session'
import VueResource from 'vue-resource'
import Toasted from 'vue-toasted';
import PulseLoader from 'vue-spinner/src/PulseLoader.vue'

library.add(faUserSecret)

Vue.config.productionTip = false
Vue.directive('scroll', {
    inserted: function (el, binding) {
        let f = function (evt) {
            if (binding.value(evt, el)) {
                window.removeEventListener('scroll', f)
            }
        }
        window.addEventListener('scroll', f)
    }
})

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(BootstrapVue)
Vue.use(Toasted)
Vue.use(VueResource)
Vue.use(VueSession)
Vue.use(VueExpandableImage)

Vue.prototype.$appName = 'ShoClean';
Vue.prototype.$apiUrl = 'http://localhost:8000/v1/';

new Vue({
    router,
    render: h => h(App),
    http: {
        root: '/root',
        headers: {
            Authorization: 'Basic YXBpOnBhc3N3b3Jk'
        }
    },
    components: {
        PulseLoader
    }

}).$mount('#app');
