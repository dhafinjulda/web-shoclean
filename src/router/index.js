import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '../views/Homepage.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
            path: '/',
            name: 'home',
            component: Homepage
        },
        {
            path: '/AboutUs',
            name: 'AboutUs',
            component: () =>
                import ('../views/AboutUs')
        },
        {
            path: '/Partners',
            name: 'Partners',
            component: () =>
                import ('../views/Partners')
        },
        {
            path: '/registerPartner',
            name: 'RegisterPartner',
            component: () =>
                import ('../views/RegisterPartner')
        },
        {
            path: '/orderCreate',
            name: 'OrderCreate',
            component: () =>
                import ('../views/OrderCreate')
        },
        {
            path: '/service',
            name: 'Service',
            component: () =>
                import ('../views/Service')
        },
        {
            path: '/orderLocation',
            name: 'OrderLocationForm',
            component: () =>
                import ('../views/OrderLocationForm')
        },
        {
            path: '/mitra/listCreateInvoice',
            name: 'ListCreateInvoice',
            component: () =>
                import ('../views/mitra/ListCreateInvoice')
        },
        {
            path: '/mitra/createInvoice',
            name: 'CreateInvoice',
            component: () =>
                import ('../views/mitra/CreateInvoice')
        },
        {
            path: '/mitra/reportCustomer',
            name: 'reportInvoice',
            component: () =>
                import ('../views/mitra/reportInvoice')
        },
        {
            path: '/mitra/reportService',
            name: 'ReportService',
            component: () =>
                import ('../views/mitra/ReportService')
        },
        {
            path: '/mitra/reportReceipt',
            name: 'ReportReceipt',
            component: () =>
                import ('../views/mitra/ReportReceipt')
        },
        {
            path: '/mitra/reportPickupDelivery',
            name: 'ReportPickupDelivery',
            component: () =>
                import ('../views/mitra/ReportPickupDelivery')
        },
        {
            path: '/mitra/reportComplain',
            name: 'reportInvoice',
            component: () =>
                import ('../views/mitra/reportInvoice')
        },
        {
            path: '/mitra/reportInvoice',
            name: 'reportInvoice',
            component: () =>
                import ('../views/mitra/reportInvoice')
        },
        {
            path: '/mitra/listCreatePickup',
            name: 'ListCreatePickup',
            component: () =>
                import ('../views/mitra/ListCreatePickup')
        },
        {
            path: '/mitra/createPickup',
            name: 'createPickup',
            component: () =>
                import ('../views/mitra/createPickup')
        },
        {
            path: '/mitra/listCreateDelivery',
            name: 'ListCreateDelivery',
            component: () =>
                import ('../views/mitra/ListCreateDelivery')
        },
        {
            path: '/mitra/createDelivery',
            name: 'CreateDelivery',
            component: () =>
                import ('../views/mitra/CreateDelivery')
        },
        {
            path: '/mitra/listCreateReceipt',
            name: 'ListCreateReceipt',
            component: () =>
                import ('../views/mitra/ListCreateReceipt')
        },
        {
            path: '/mitra/createReceipt',
            name: 'CreateReceipt',
            component: () =>
                import ('../views/mitra/CreateReceipt')
        },
        {
            path: '/customer/listOrder',
            name: 'ListOrder',
            component: () =>
                import ('../views/customer/ListOrder')
        },
    ]
})